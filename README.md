# check_run

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/check_run.svg)](https://forge.puppetlabs.com/landcaresearch/check_run)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetCheckrun_Build%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetCheckrun_Build&guest=1)

## Overview

Manages tasks checking if a task has run or if a task should run.

## Module Description

Runs a command only once using a file based approach to indicate a task was run.
This is primarily useful for running a one off command for installation.

## Usage

**Make sure that check_run is included in order to use its variables**

```puppet
include check_run
$task_name = 'test_run'
```

**Runs the command only once and will not execute after this one**

```puppet
check_run::task{$task_name:
  exec_command => "/usr/bin/touch $check_run::root_dir/ttt",
}
```

## Limitations

Only works with redhat & debian based OS's.

## Development

The module is open source and available on [bitbucket](https://bitbucket.org/landcareresearch/puppet-checkrun).  Please fork!  

### Testing

The vagrant directory contains the vagrant scripts for functional testing. See the readme.md file in this directory for additional information.
