# Checkrun Puppet Module Changelog

## 2020-10-02 Version 6.0.0

- Updated changelog formatting.
- Updated to puppet 6.
- Added PDK support.

## 2017-01-20 Version 0.3.2

- Added path parameter for exec.

## 2016-01-18 Version 0.3.1

- Now compiles with puppet 4 parser.
- Added new testing framework using vagrant.
- Added Testing for Ubuntu 15.04 and Centos 7.

## 2015-03-16 Version 0.3.0

- The default root directory was defined in the check_run class; however,
   if check_run was not defined before check_run::task, than puppet would error.
   This has been fixed inside the check_run::task subclass.

## 2015-03-04 Version 0.2.5

- Migrated from github to bitbucket
- Changed ownership of puppetforge account

## 2015-01-16 Version 0.2.4

- Initial start of support for windows.
- Added Travis CI Integration

## 2014-10-17 Version 0.2.3

- Added support for redhat family
- Added anchor pattern
- Moved from Modulefile to metadata.json
- Added OS Support to metadata

## 2014-07-23 Version 0.2.2

- Created a github repo and uploaded to this repo

## 2014-04-18 Version 0.2.1

- Added cwd parameter to task to pass to exec command

## 2014-04-18 Version 0.2.0

- Removed check_run:run and incorperated the execution of the command into the task defined type

## 2014-03-6 Version 0.1.3

- Added user
- Added group
- Added the timeout paramter for exec.

## 2014-02-5 Version 0.1.2

- Change the name of the README file to be compatiable with Puppetforge 

## 2014-02-5 Version 0.1.1

- Updated License and other documents

## 2014-02-5 Version 0.1.0

- Initial Release
