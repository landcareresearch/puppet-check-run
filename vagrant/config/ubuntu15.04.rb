module MyVars
  OS     = "ubuntu/vivid64"
  OS_URL = "https://atlas.hashicorp.com/ubuntu/boxes/vivid64"
  PUPPET = "scripts/noop.sh"
end
