include check_run

$task_name = 'test_run'

case $::osfamily {
  'debian': {
    $touch_cmd_path = '/usr/bin/touch'
  }
  'redhat': {
    $touch_cmd_path = '/bin/touch'
  }
  default: {
    $touch_cmd_path = '/usr/bin/touch'
  }
}

check_run::task{$task_name:
  exec_command => "${touch_cmd_path} ${check_run::root_dir}/ttt",
}